import React, { Component } from 'react';
import Slider from './Slider';
import Featured from './Featured';
import FeaturedDestinations from './FeaturedDestinations';

class Home extends Component {
    render() {
        return (
            <div className="Home">
                <div className="Home-container">
                    <Slider />
                    <Featured />
                    <FeaturedDestinations />
                </div>
            </div>
        );
    }
}

export default Home;
