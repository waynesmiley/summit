import React, { Component } from 'react';

class Slider extends Component {
    render() {
        return (
            <div className="Slider">
                <div className="Slider-container">
                    Slider goes here.
                </div>
            </div>
        );
    }
}

export default Slider;
