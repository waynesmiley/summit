import React, { Component } from 'react';
import './nav.css';

class Nav extends Component {
    render() {
        return (
            <div className="Nav">
                <div className="Nav-container">
                    <ul className="main-nav">
                        <li><a href="/quick-trips">Quick Trips</a></li>
                        <li><a href="/destinations">Destinations</a>
                            <ul className="main-nav sub-nav">
                                <li><a href="/greater-yellowstone">Greater Yellowstone</a></li>
                                <li><a href="/pacific-northwest">Pacific Northwest</a></li>
                                <li><a href="/utah">Utah</a></li>
                            </ul>
                        </li>
                        <li><a href="/affiliates">Affiliates</a></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Nav;
