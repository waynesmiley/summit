import React, { Component } from 'react';
import './nav.css';
import Menu, { SubMenu, MenuItem } from 'rc-menu';

class Nav extends Component {
    render() {
        return (
            <div className="Nav">
                <div className="Nav-container">
                    <Menu>
                        <MenuItem><a href="/quick-trips">Quick Trips</a></MenuItem>
                        <SubMenu title="Destinations">
                            <MenuItem><a href="/greater-yellowstone">Greater Yellowstone</a></MenuItem>
                            <MenuItem><a href="/pacific-northwest">Pacific Northwest</a></MenuItem>
                            <MenuItem><a href="/utah">Utah</a></MenuItem>
                        </SubMenu>
                        <MenuItem><a href="/affiliates">Affiliates</a></MenuItem>
                    </Menu>
                </div>
            </div >
        );
    }
}

export default Nav;
