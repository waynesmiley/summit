import React, { Component } from 'react';
import logo from './logo-summit.png';
import './header.css';
import Nav from './Nav/Nav';

class Header extends Component {
  render() {
    return (
      <div className="Header">
        <div className="Header-container">
          <img src={logo} className="Header-logo" alt="logo" />
          <Nav />
        </div>
      </div>
    );
  }
}

export default Header;
