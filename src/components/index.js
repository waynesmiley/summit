import React from 'react'
import styled from 'styled-components'

export const Destination = ({ dest: { title, text, img } }) => (
  <ImageWrapper>
    <img alt="" src={img} />
    <ImageTitle>{title}</ImageTitle>
    <ImageText>{text}</ImageText>
  </ImageWrapper>
)


const ImageWrapper = styled.div`

`

const ImageTitle = styled.h4`

`

const ImageText = styled.p`

`
