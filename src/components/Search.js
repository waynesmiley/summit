import React, { Component } from 'react';

class Search extends Component {
    constructor(props) {
        super(props);

        this.state = { destinations: [] };

        // This is where the search will go
        YTSearch({ key: API_KEY, term: 'Utah' }, (destinations) => {
            this.setState({ destinations });
        });
        // I'll need to implement a blank search option for 10-12 that
        // takes whatever the client types in and searches for it
    }

    render() {
        return (
            <div>
                <SearchBar />
            </div>
        );
    }
}

ReactDOM.render(<Search />, document.querySelector('.contianer'));