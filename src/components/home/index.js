import React, { Fragment } from 'react'

import Picks from './Picks'

export default () => (
  <Fragment>
    <Picks />
  </Fragment>
)
