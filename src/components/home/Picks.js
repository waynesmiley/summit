import React, { Component } from 'react'
import styled from 'styled-components'

import { Destination } from '../../components'

export default class Something extends Component {
  constructor(props) {
    super(props)

    this.destinations = [
      {
        img: 'cascades.png',
        title: 'Cascades backpacking, WA',
        text: 'throw in some dummy text'
      },
      {
        img: 'hunting.png',
        title: 'Cascades backpacking, AZ',
        text: 'throw in some dummy text'
      }
    ]
  }

  render() {
    let { destinations } = this

    return (
      <Wrapper>
        <SectionHeader>Best hikes</SectionHeader>
        {
          destinations.map(dest => (
            <Destination key={dest.title} dest={dest} />
          ))
        }
      </Wrapper>
    )
  }
}


const Wrapper = styled.div`

`

const SectionHeader = styled.h2`

`
